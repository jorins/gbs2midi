defmodule Gbs2Midi.MixProject do
  use Mix.Project

  def project do
    [
      app: :gbs2midi,
      version: "0.1.0",
      elixir: "~> 1.8",
      escript: escript(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    []
  end

  def escript do
    [
      main_module: Gbs2Midi.CLI,
      path: "bin/gbs2midi"
    ]
  end

  defp deps do
    [
      {:loggy, "~> 0.1.1"},
      {:deep_merge, "~> 0.2.0"}
    ]
  end
end
