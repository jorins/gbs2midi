defmodule Gbs2Midi.Midi do
  alias Loggy, as: L

  def translate(gbs_dat, _opts) do
    L.info("MIDI goes nyoom over gbs data that's #{String.length(gbs_dat)} long.")
    {:ok, gbs_dat}
  end
end
