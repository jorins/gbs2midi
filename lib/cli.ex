defmodule Gbs2Midi.CLI do
  alias Loggy, as: L

  @compile_time DateTime.utc_now() |> DateTime.truncate(:second) |> DateTime.to_naive()
  @version Gbs2Midi.MixProject.project()[:version]
  @argv_opts [
    {:debug, :d, :boolean, "print debugging information"},
    {:help, :h, :boolean, "print this message and exit"},
    # {:serial, :s, :boolean, "process files serially (parallel by default)"},
    {:verbose, :v, :boolean, "print verbose information"},
    {:version, :V, :boolean, "print version statement and exit"}
  ]
  @errors [
    enoent: "Could not read file %s. File does not exist.",
    eaccess: "Could not read file %s. Lacking permissions.",
    eisdir: "Could not read file %s. Path is a directory.",
    enotdir: "Could not read file %s. One of the path components is not a directory.",
    enomem: "Could not read file %s. Not enough memory."
  ]

  def main(argv) do
    {parsed, target_files, rest} = parse_args(argv)
    L.set_user_opts(parsed)
    L.info("Running in verbose mode.", verbose: true)
    L.debug("Running in debug mode.")
    L.debug("Parsed options: #{inspect(parsed)}")
    L.debug("Targets files: #{inspect(target_files)}")
    warn_rest(rest)

    cond do
      parsed[:version] ->
        IO.puts("gbs2midi v.#{@version} (compiled #{@compile_time} UTC)")

      parsed[:help] || target_files == [] ->
        opts_help =
          for {key, short, _, message} <- @argv_opts,
              key = Atom.to_string(key),
              short = Atom.to_string(short) do
            "  -#{short}, --#{key}: #{message}\n"
          end
          |> Enum.join()

        IO.puts("""

        Usage: gbs2midi [options] targets...
        Options:
        #{opts_help}
        Targets should be .gbs files.
        """)

      true ->
        run(target_files, parsed)
    end
  end

  defp run([], _opts) do
    L.info("Done!")
  end

  defp run([target | rest], opts) do
    run_one(target, opts)
    run(rest, opts)
  end

  defp run_one(target, opts) do
    L.info("Processing #{target}", verbose: true)

    with {:ok, contents} <- File.read(target),
         {:ok, gbs_dat} <- Gbs2Midi.Gbs.translate(contents, opts),
         {:ok, midi_dat} <- Gbs2Midi.Midi.translate(gbs_dat, opts) do
      L.info(midi_dat)
    else
      error -> explain_error(target, error)
    end
  end

  defp warn_rest([]) do
  end

  defp warn_rest([{key, _val} | rest]) do
    L.warn("Unknown option: #{key}")
    warn_rest(rest)
  end

  defp parse_args(argv) do
    opts =
      for {key, key_alias, type, help_message} <- @argv_opts do
        [
          strict: [{key, type}],
          aliases: [{key_alias, key}],
          help_messages: [{key, help_message}]
        ]
      end
      |> Enum.reduce([], &DeepMerge.deep_merge/2)

    OptionParser.parse_head(argv, opts)
  end

  defp explain_error(target, {:error, error}) do
    @errors
    |> Keyword.get(error, "Unknown error while processing file %s: #{error}")
    |> String.replace("%s", target)
    |> L.error()
  end

  defp explain_error(target, unknown) do
    L.error("Unknown error while processing file #{target}: #{inspect(unknown)}")
  end
end
