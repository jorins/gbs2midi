defmodule Gbs2Midi.Gbs do
  alias Loggy, as: L

  def translate(raw, _opts) do
    L.info("Pretend I'm processing something that's #{String.length(raw)} long.")
    {:ok, raw}
  end
end
